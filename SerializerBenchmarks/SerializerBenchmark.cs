﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using otus.reflection;
using otus.reflection.Serializers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace SerializerBenchmarks
{
    
    public class SerializerBenchmark
    {
        private string _csv;
        private F _f;
        private string _json;

        [Params(1000, 1000000)]
        public int K;

        [GlobalSetup]
        public void Setup()
        {
            _csv = "i1,i2,i3,i4,i5" + Environment.NewLine + "1,2,3,4,5";
            _f = new F().Get();
            _json = Serializer<F>.Serialize(_f);

        }

        [Benchmark]
        public void Csv_Deserialization()
        {
            for (int i = 0; i < K; i++)
            {
                SerialiserCSV<F>.Deserialize(_csv);
            }
        }

        [Benchmark]
        public void My_Json_Serialization()
        {
            for (int i = 0; i < K; i++)
            {
                Serializer<F>.Serialize(_f);
            }
        }

        [Benchmark]
        public void JsonSerializer_Serialization()
        {
            for (int i = 0; i < K; i++)
            {
                JsonSerializer.Serialize(_f);
            }
        }

        [Benchmark]
        public void My_Json_Deserialization()
        {
            for (int i = 0; i < K; i++)
            {
                Serializer<F>.Deserialize(_json);
            }
        }

        [Benchmark]
        public void JsonSerializer_Deserialization()
        {
            for (int i = 0; i < K; i++)
            {
                JsonSerializer.Deserialize<F>(_json);
            }
        }



    }
}
