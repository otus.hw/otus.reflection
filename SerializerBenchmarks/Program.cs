﻿using BenchmarkDotNet.Running;
using System;

namespace SerializerBenchmarks
{
    class Program
    {
        static void Main(string[] args)
        {
            BenchmarkRunner.Run<SerializerBenchmark>();
        }
    }
}
