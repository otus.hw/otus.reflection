﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace otus.reflection.Serializers
{
    public static class Serializer<T> where T : class, new()
    {

        public static string Serialize(T obj)
        {

            PropertyInfo[] _properties = obj.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);
            StringBuilder sb = new StringBuilder();
            sb = sb.Append("{");
            foreach (PropertyInfo _field in _properties)
            {
                sb = sb.Append("\""+_field.Name + "\":" + _field.GetValue(obj) + ",");
            }

            sb.Remove(sb.Length - 1, 1);
            sb.Append("}");
            return sb.ToString();
        }

        public static T Deserialize(string data)
        {
            var obj = new T();
            data = data.Replace("{", "").Replace("}", "");
            var splitedData = data.Split(",");

            PropertyInfo[] _properties = obj.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);
            var keyValue = new Dictionary<string, string>();

            foreach (var item in splitedData)
            {
                var spliteditem = item.Split(":");
                if (spliteditem.Count() > 2)
                    throw new ArgumentException($"Wrong key value pair {item}");

                PropertyInfo p = _properties.SingleOrDefault(x => x.Name == spliteditem[0].Replace("\"", ""));

                if (p == null)
                    continue;

                var value = Convert.ChangeType(spliteditem[1], p.PropertyType);
                p.SetValue(obj, value);

            }

            return obj;
        }

    }
}
