﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace otus.reflection.Serializers
{
    public static class SerialiserCSV<T> where T : class, new()
    {
        public static T Deserialize(string data)
        {
            var obj = new T();

            var csvData = data.Split(Environment.NewLine);
            if (csvData?.Length != 2)
            {
                throw new ArgumentException("CSV-string wrong format");
            }


            var csvKey = csvData[0].Split(",");
            var csvValue = csvData[1].Split(",");

            if (csvKey.Length != csvValue.Length)
                throw new ArgumentException($"Number kye not equal number value ");

            PropertyInfo[] _properties = obj.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);

            for (var i = 0; i<csvValue.Length; i++)
            {
                PropertyInfo p = _properties.SingleOrDefault(x => x.Name == csvKey[i]);

                if (p == null)
                    continue;

                var value = Convert.ChangeType(csvValue[i], p.PropertyType);
                p.SetValue(obj, value);

            }

            return obj;
        }
    }
}
